# Todo Test

Todo app test 

- [X] Quality on Business Value (Acceptance tests)
- [X] Quality on Performance (load tests, performance tests)

Into wdio.conf to run Scenario added @Run tagExpression. 
The Scenario we want to run this tag needs to be added at the beginning.

 ## Allure Report 
It is necessary to remove the allure report for field from the comment line in **`wdio.conf`**

    npm run allure-report

![Screen_Shot_2022-08-04_at_16.36.44](/uploads/c944d680956fb944c18a51b546bb6c8c/Screen_Shot_2022-08-04_at_16.36.44.png)
![Screen_Shot_2022-08-04_at_16.36.19](/uploads/83441a25025f8941723f62ce34258fd3/Screen_Shot_2022-08-04_at_16.36.19.png)
 ## Spec Report
It is necessary to remove the spec for field from the comment line in **`wdio.conf`**
    
    npm run wdio
    
![image](/uploads/c44954a9f252abfd4d8effaf33db4853/image.png)
 ## k6 Test
![Screen_Shot_2022-08-04_at_13.40.52_1](/uploads/43852f5be821bf0844c1aa462265b4d9/Screen_Shot_2022-08-04_at_13.40.52_1.png)
