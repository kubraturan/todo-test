const Page = require('../pageobjects/page');

class Todo extends Page {
    constructor() {
        super();
        this.open();
    }

    // Todolistesindeki todoları getirir.
    get todoList() {
        return $$('ul[class="todo-list"] li');
    }

    // yeni todoItem eklenecek olan input alanı
    get todoInput() {
        return $('input[class="new-todo"]');
    }

    // todolistesinden ilgili index numaralı todoItem'ı getirir
    todoListChildItem(index) {
        return $(`ul[class="todo-list"] li:nth-child(${index})`);
    }

    // todoItemı okundu işaretlemek için kullanılan inputu getirir
     todoListChildItemInput(index) {
        return $(`ul[class="todo-list"] li:nth-child(${index}) div input`);
     }

     // todoItem'ı silmeye yarayan button
     todoListChildItemDeleteButton(index) {
        return $(`ul[class="todo-list"] li:nth-child(${index}) div button[class="destroy"]`);
     }

    // todolistesindeki son todoItemın labelini getirir.
    get todoListLastChild() {
        return $('ul[class="todo-list"] li:last-child div label');
    }

    /**
     * todoListesi boş mu kontrol eder.
     */
    async emptyTodoListCheck() {
        const todoList = await this.todoList;
        expect(todoList.length).toEqual(0);
    }

    /**
     * Yeni todo ekler
     * @param todo eklenecek olan todo
     */
    async addNewTodo(todo) {
        await this.todoInput.setValue(todo);
        await browser.keys("Enter");
        await browser.pause(1000);
    }

    /**
     * Todoyu siler.
     * @param todoItem silinecek olan todo
     */
    async deleteTodo(todoItem) {
        await this.checkTodoListInTodoItemExist(todoItem).then(async (value) => {
            const index = value.index;
            const todoListChildItem = this.todoListChildItem(index);
            const todoListChildItemDeleteButton = await this.todoListChildItemDeleteButton(index);
            await todoListChildItem.moveTo();
            await todoListChildItemDeleteButton.click();
            await  browser.pause(1000);
        })
    }
    /**
     * Todo listesindeki en son elemana bakar yeni eklenen todo o mu ?
     * @param todo kontrol edilecek olan todo
     */
    async checkTodoListInTodoItemLastChildExist(todo) {
        const todoItemContent = await this.todoListLastChild.getText();
        expect(todoItemContent).toContain(todo);
    }

    /**
     * Todo listesinde parametredeki todo var mı bakar ?
     * @param todo kontrol edilecek olan todo
     */
    checkTodoListInTodoItemExist(todo) {
        return new Promise(async (resolve) => {
            const todoList = await this.todoList;
            var todoListItemTextList = [];

            if (todoList.length > 0) {
                todoList.map(async (todoListItem,index) => {
                    let elementId = todoListItem.elementId;
                    let newIndex = index !== 0 ? index : index + 1;
                    await todoListItem.getElementText(elementId).then((todoListItemText) => todoListItemTextList.push(todoListItemText));

                    if(todoListItemTextList.length === todoList.length) {
                        const isIncludeTodoItem = todoListItemTextList.includes(todo);
                        resolve({
                            isIncludeTodoItem,
                            index: newIndex
                        })
                    }
                })
            }
            else {
                // liste sıfırdan küçükse false dön
                resolve({
                    isIncludeTodoItem: false,
                })
            }
        })
    }

    /**
     * Todo listesinde bu todoItem var mı?
     * @param todoItem bakılacak olan todoItem
     */
    async checkTodoItemExist(todoItem) {
        await this.checkTodoListInTodoItemExist(todoItem).then(async (value) => {
            const isIncludeTodo = value.isIncludeTodoItem;
            expect(isIncludeTodo).toEqual(true);
            await browser.pause(1000);
        })
    }

    /**
     * Todolistesinde todoItem var mı kontrol et eğer yoksa todoyu listeye ekle
     * @param todoItem kontrol edelicek yoksa eklenecek olan todoItem
     */
    async checkTodoItemIsExistAddTodo(todoItem) {
        await this.checkTodoListInTodoItemExist(todoItem).then(async (value) => {
            let isIncludeTodo = value.isIncludeTodoItem;
            if (!isIncludeTodo) {
                // eğer false ise todoyu ekle
                await this.addNewTodo(todoItem);
                isIncludeTodo = true;
            }
            expect(isIncludeTodo).toEqual(true);
            await browser.pause(5000);
        })
    }

    /**
     * todolistesinden todoItemı bul ve onu okundu işaretle
     * @param todoItem okundu işaretlenecek todo
     */
    async clickAndCheckedTodoItem(todoItem) {
        await this.checkTodoListInTodoItemExist(todoItem).then(async (value) => {
            const todoItemInput = await this.todoListChildItemInput(value.index);
            await todoItemInput.click();
        })
    }

    /**
     * todolistesinde todoItem işaretlimi işaretli değil mi kontrol et
     * @param todoItem kontrol edilecek todo
     * @param isCheck işaretli - işaretli değil kontrolü için değer
     */
    async isTodoItemCheckedCheck(todoItem, isCheck) {
        await this.checkTodoListInTodoItemExist(todoItem).then(async (value) => {
            const todoItemInput = await this.todoListChildItem(value.index);
            const todoItemClassname = await todoItemInput.getAttribute("class");
            const isIncludeCompleted = todoItemClassname.includes("completed");

            await browser.pause(1000);

            expect(isIncludeCompleted).toEqual(isCheck);
        })
    }

    /**
     * testi yapılacak siteyi açar
     */
    open() {
        return super.open('https://todomvc.com/examples/vue/');
    }
}

module.exports = new Todo();