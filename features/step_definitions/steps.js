const {Given, When, Then} = require("@wdio/cucumber-framework");
const todoObjects = require('../pageobjects/todo');

Given(/^Empty ToDo list$/, async function () {
    await todoObjects.emptyTodoListCheck();
});

Given(/^ToDo list with (.*) item$/, async function (todoItem) {
    await todoObjects.checkTodoItemIsExistAddTodo(todoItem);
});

Given(/^ToDo list with marked item.$/, async function () {
    // buy some milk işaretli mi bak ?
    let todoItem = "buy some milk";
    await todoObjects.checkTodoItemIsExistAddTodo(todoItem);
    await todoObjects.clickAndCheckedTodoItem(todoItem);
    await todoObjects.isTodoItemCheckedCheck(todoItem, true);
});

Given(/^ToDo list with (.*) and (.*) item in order$/, async function (newTodoItem, todoItem) {
    await todoObjects.checkTodoItemIsExistAddTodo(newTodoItem);
    await todoObjects.checkTodoItemIsExistAddTodo(todoItem);
});

When(/^I write (.*) to (.*) and press (.*)$/, async function (todoItem, textbox, enter) {
    await todoObjects.addNewTodo(todoItem);
});

When(/^I click on (.*) next to (.*) and press (.*)$/, async function (checkbox, todoItem, enter) {
    await todoObjects.clickAndCheckedTodoItem(todoItem);
});

When(/^I click on (.*) next to item$/, async function (checkbox) {
    let todoItem = "buy some milk"
    await todoObjects.clickAndCheckedTodoItem(todoItem);
});

When(/^I click on (.*) next to (.*) item$/, async function (deleteButton, todoItem) {
    if (deleteButton === "<delete-button>") {
        await todoObjects.deleteTodo(todoItem)
    }
    else if (deleteButton === "<checkbox>") {
        await todoObjects.clickAndCheckedTodoItem(todoItem)
    }
});

Then(/^I should see (.*) item in ToDo list$/, async function (todoItem) {
    await todoObjects.checkTodoListInTodoItemLastChildExist(todoItem);
});

Then(/^I should see (.*) item insterted to ToDo list below (.*) item$/, async function (newTodoItem, todoItem) {
    await todoObjects.checkTodoItemExist(newTodoItem);
    await todoObjects.checkTodoItemExist(todoItem);
});

Then(/^I should see (.*) item marked as DONE$/, async function (todoItem) {
    await todoObjects.isTodoItemCheckedCheck(todoItem, true);
});

Then(/^I should see (.*) item marked as UNDONE$/, async function (todoItem) {
    // UNDONE ile işaretlenmiş olması lazım
    await todoObjects.isTodoItemCheckedCheck(todoItem, false);
});

Then(/^List should be empty$/, async function () {
    await todoObjects.emptyTodoListCheck();
});