Feature: Todo Acceptance
  Scenario Outline: Verify that item can be added to the to-do list
    Given Empty ToDo list
    When I write <todoItem> to <textbox> and press <enter>
    Then I should see <todoItem> item in ToDo list
    Examples:
      | todoItem      |
      | buy some milk |
  Scenario Outline: Verify that items can be added to the to-do list
    Given ToDo list with <todoItem> item
    When I write <newTodoItem> to <textbox> and press <enter>
    Then I should see <newTodoItem> item insterted to ToDo list below <todoItem> item
    Examples:
      | todoItem      | newTodoItem          |
      | buy some milk | enjoy the assignment |

  Scenario Outline: Verify that the item on the to-do list has been done
    Given ToDo list with <todoItem> item
    When I click on <checkbox> next to <todoItem> item
    Then I should see <todoItem> item marked as DONE
    Examples:
      | todoItem      |
      | buy some milk |

  Scenario Outline: Verify that the item on the to-do list has been undone
    Given ToDo list with marked item.
    When I click on <checkbox> next to item
    Then I should see <todoItem> item marked as UNDONE
    Examples:
      | todoItem      |
      | buy some milk |
  @Run
  Scenario Outline: Verify that the item in the to-do list can be deleted
    Given ToDo list with <todoItem> item
    When I click on <delete-button> next to <todoItem> item
    Then List should be empty
    Examples:
      | todoItem         |
      | rest for a while |

  Scenario Outline: Verify that only one item in the to-do list can be deleted
    Given ToDo list with <todoItem> and <newItem> item in order
    When I click on <delete button> next to <todoItem> item
    Then I should see <newItem> item in ToDo list
    Examples:
      | todoItem         | newItem     |
      | rest for a while | drink water |
