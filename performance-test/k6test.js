import http from 'k6/http';
import { check } from 'k6';

import { Rate } from 'k6/metrics';

export let errorRate = new Rate('errors');

export let options = {
    thresholds : {
        errors: ['rate<0.1'],
        // kullanıcıların yüzden 90 ını 1500 ms altında yanıt alıyor mu ?
        http_req_duration: ['p(90)<1500']
    },

    stages: [
        { target: 10, duration: '5s'}, // 5 sn boyunca 10 kullanıcıya çık
        { target: 10, duration: '10s'}, // 10 sn boyunca 10 kullanıcıda bekle (15.saniyeye kadar)
        { target: 30, duration: '10s' }, // 10 sn boyunca  30 kullanıcıya çık (25.saniyeye kadar)
        { target: 0, duration: '5s' }, // 5 sn boyunca kullanıcı sayısını sıfıra indir (30.saniyeye kadar)
    ]
}

export default function(){
    let responce = http.get('https://todomvc.com/examples/vue/')

    const check1 = check(responce, {
        'is responce status is 200 :' : (r)=> r.status === 200,
        'is not status 404': r => r.status !== 404,
        'page title todos': (r) => r.html('h1').text().includes('todos'),
    })
    errorRate.add(!check1);
}